import logo from './logo.svg'
import './App.css'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Test deploy heroku to production deploy
        </a>
        <p>Secret Key {process.env.REACT_APP_SECRET_KEY}</p>
      </header>
    </div>
  )
}

export default App
